bamboo_firewall_setup:
  firewalld.present:
    - name: public
    - default: True
    - masquerade: False
    - ports:
      - 22/tcp
      - 4505/tcp
      - 4506/tcp
      - 8085/tcp
bamboo_firewall_bind:
  firewalld.bind:
    - name: public
    - interfaces:
      - eth0
    - sources:
      - 10.211.55.0/24
