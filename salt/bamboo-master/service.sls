bamboo_master_init_script:
  file.managed:
    - template: jinja
{% if salt['grains.get']('init') == 'systemd' %}
    - name: /etc/systemd/system/bamboo-master.service
    - source: salt://bamboo-master/files/systemd_unit_script
{% else %}
    - name: /etc/init.d/bamboo-master
    - source: salt://bamboo-master/files/init_script
{% endif %}

bamboo_master_service:
  service.running:
    - name: bamboo-master
    - enable: True
    - reload: False
    - watch: 
      - sls: bamboo-master.install
      - file: bamboo_master_init_script
    - require:
      - sls: bamboo-master.install
      - file: bamboo_master_init_script
