{% set bamboo_master_version = salt['pillar.get']('bamboo_master_version') %}
{% set bamboo_master_symlink = salt['pillar.get']('bamboo_master_symlink') %}
{% set bamboo_master_user = salt['pillar.get']('bamboo_master_user') %}
{% set bamboo_master_group = salt['pillar.get']('bamboo_master_group') %}
bamboo_master_package:
  pkg.installed:
    - name: bamboo-master
    - version: {{ bamboo_master_version }}-1

bamboo_master_symlink:
  file.symlink:
    - name: {{ bamboo_master_symlink }}
    - target: /opt/bamboo/atlassian-bamboo-{{ bamboo_master_version }}
    - require: 
      - pkg: bamboo_master_package

bamboo_master_init_properties:
  file.managed:
    - name: {{ bamboo_master_symlink }}/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties
    - source: salt://bamboo-master/files/bamboo-init.properties
    - user: {{ bamboo_master_user }}
    - group: {{ bamboo_master_group }}
    - template: jinja
    - require:
      - file: bamboo_master_symlink
