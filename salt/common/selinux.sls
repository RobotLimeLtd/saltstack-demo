{% set selinux_mode = salt['pillar.get']('selinux_mode') %}
setup_selinux:
  selinux.mode:
    - name: {{ selinux_mode }}
