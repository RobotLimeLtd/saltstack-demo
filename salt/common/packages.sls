{% for package in salt['pillar.get']('packages') %}
  {{ package }}_install:
    pkg.installed:
      - name: {{ package }}
{% endfor %}
