local_yum_repo:
  pkgrepo.managed:
    - name: local
    - humanname: CentOS-$releasever - local packages for $basearch
    - baseurl: file:///srv/CentOS/$releasever/local/$basearch
    - gpgcheck: 0
    - metadata_expire: 30s

