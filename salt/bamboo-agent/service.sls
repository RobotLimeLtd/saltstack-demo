bamboo_agent_init_script:
  file.managed:
{% if salt['grains.get']('init') == 'systemd' %}
    - name: /etc/systemd/system/bamboo-agent.service
    - source: salt://bamboo-agent/files/unit_script
{% else %}
    - name: /etc/init.d/bamboo-agent
    - source: salt://bamboo-agent/files/init_script
{% endif %}

bamboo_agent_service:
  service.running:
    - name: bamboo-agent
    - enable: True
    - reload: False
    - watch: 
      - sls: bamboo-agent.install
      - file: bamboo_agent_init_script
    - require:
      - sls: bamboo-agent.install
      - file: bamboo_agent_init_script
