{% set bamboo_master_version = salt['pillar.get']('bamboo_master_version') %}
{% set bamboo_master_url = salt['pillar.get']('bamboo_master_url') %}

bamboo_agent_user:
  user.present:
    - name: bamboo
    - home: /opt/bamboo

bamboo_agent_jar:
  file.managed:
    - name: /opt/bamboo/atlassian-bamboo-agent-installer-{{ bamboo_master_version }}.jar
    - source: {{ bamboo_master_url }}/agentServer/agentInstaller/atlassian-bamboo-agent-installer-{{ bamboo_master_version }}.jar
    - skip_verify: True
    - user: bamboo
    - group: bamboo
    - mode: 755

bamboo_agent_install:
  cmd.run:
    - name: java -Dbamboo.home=/opt/bamboo/bamboo-agent-home -jar atlassian-bamboo-agent-installer-{{ bamboo_master_version }}.jar {{ bamboo_master_url }}/agentServer/ install
    - cwd: /opt/bamboo
    - runas: bamboo
    - creates: /opt/bamboo/bamboo-agent-home/installer.properties
    - require:
      - file: bamboo_agent_jar
