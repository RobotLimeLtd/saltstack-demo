nfs_server:
  service.running:
    - name: nfs-server
    - enable: True

exports_file:
  file.managed:
    - name: /etc/exports
    - source: salt://nfs-server/files/etc-exports
    - owner: root
    - group: root
    - perm: 644
  
exportfs_cmd:
  cmd.run:
    - name: "exportfs -a"
    - require: 
      - file: exports_file
    - onchanges:
      - file: exports_file
