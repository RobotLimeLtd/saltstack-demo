base:
  '*':
    - common

  'salt-master*':
    - nfs-server
    - bamboo-master

  'salt-minion*':
    - minion-only
    - bamboo-agent
