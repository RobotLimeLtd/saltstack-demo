yum_mount_point:
  file.directory:
    - name: /srv/CentOS

yum_nfs_mount:
  mount.mounted:
    - device: salt:/srv/CentOS
    - name: /srv/CentOS
    - fstype: nfs4
    - require:
      - file: yum_mount_point
