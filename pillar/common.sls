packages:
  - policycoreutils
  - policycoreutils-python
  - libcurl-devel
  - telnet
  - screen
  - git
  - ruby
  - rubygems
  - autofs
  - nfs-utils
  - ntp
  - java-1.8.0-openjdk

bamboo_master_version: 5.12.3.1

bamboo_master_url: http://10.211.55.26:8085

selinux_mode: permissive
